//! # win-idispatch
//!
//! This crate is a utility crate to improve the ergonmics of working with
//! IDispatch interfaces with windows COM servers.
mod dispatch;
mod errors;
mod guids;
mod unknown;

pub use dispatch::DispatchInterface;
pub use errors::{Error, HResultError};
pub use unknown::UnknownInterface;
